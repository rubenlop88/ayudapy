package org.ayudapy.db

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import org.ayudapy.api.HelpRequest

@Entity
data class SavedItem(
    @PrimaryKey var id: Int,
    var title: String,
    var message: String,
    var name: String,
    var phone: String,
    var address: String,
    var city: String,
    var latitude: Double,
    var longitud: Double,
    var active: Boolean,
    var added: String
) {
    @Ignore
    constructor(helpRequest: HelpRequest) : this(
        helpRequest.id,
        helpRequest.title,
        helpRequest.message,
        helpRequest.name,
        helpRequest.phone,
        helpRequest.address,
        helpRequest.city,
        helpRequest.location.coordinates[0],
        helpRequest.location.coordinates[1],
        helpRequest.active,
        helpRequest.added
    )
}