package org.ayudapy.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [SavedItem::class, CompletedItem::class], version = 2)
abstract class AppDatabase : RoomDatabase() {

    companion object {
        private lateinit var database: AppDatabase

        fun init(context: Context) {
            database = Room.databaseBuilder(context, AppDatabase::class.java, "database")
                .fallbackToDestructiveMigration()
                .build()
        }

        fun get(): AppDatabase {
            return database
        }
    }

    abstract fun appDao(): AppDao
}