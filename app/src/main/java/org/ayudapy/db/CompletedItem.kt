package org.ayudapy.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CompletedItem(@PrimaryKey var id: Int)