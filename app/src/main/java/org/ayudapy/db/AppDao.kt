package org.ayudapy.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AppDao {

    @Query("SELECT * FROM saveditem")
    fun getSavedItems(): LiveData<List<SavedItem>>

    @Query("SELECT * FROM saveditem WHERE id = :id")
    fun getSavedItem(id: Int): LiveData<SavedItem?>

    @Insert
    suspend fun insert(item: SavedItem)

    @Update
    suspend fun update(item: SavedItem)

    @Delete
    suspend fun delete(item: SavedItem)

    @Query("SELECT * FROM completeditem")
    fun getCompletedItems(): LiveData<List<CompletedItem>>

    @Query("SELECT * FROM completeditem WHERE id = :id")
    fun getCompletedItem(id: Int): LiveData<CompletedItem?>

    @Insert
    suspend fun insert(item: CompletedItem)

    @Delete
    suspend fun delete(item: CompletedItem)

}