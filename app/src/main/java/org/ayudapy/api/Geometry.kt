package org.ayudapy.api

data class Geometry(var type: String, var coordinates: List<Double>)