package org.ayudapy.api

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface Api {

    @GET("helprequestsgeo/")
    fun getHelpRequests(@Query("in_bbox") boundaryBox: String): Call<HelpRequests>

    @GET("helprequests/{pk}")
    fun getHelpRequest(@Path("pk") primaryKey: Int): Call<HelpRequest>
}

