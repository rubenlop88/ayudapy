package org.ayudapy.api

data class HelpRequests(var type: String, var features: List<Feature>)