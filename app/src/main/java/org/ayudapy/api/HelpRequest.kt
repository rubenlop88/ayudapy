package org.ayudapy.api

import java.text.SimpleDateFormat
import java.util.*

data class HelpRequest(
    var id: Int,
    var title: String,
    var message: String,
    var name: String,
    var phone: String,
    var address: String,
    var city: String,
    var location: Geometry,
    var active: Boolean,
    var added: String,
    var addedFormatted: String = "" // added to avoid lose the 'added' value with formatting
) {
    companion object {
        fun dateFormat(): SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ", Locale.getDefault())
    }
}