package org.ayudapy.api

data class Properties(var pk: Int, var title: String, var name: String, var added: String)