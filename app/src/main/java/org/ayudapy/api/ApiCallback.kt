package org.ayudapy.api

import android.util.Log
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import org.ayudapy.AyudaPyApplication
import org.ayudapy.R
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ApiCallback<T>(private val liveDate: MutableLiveData<T>) : Callback<T> {

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            liveDate.value = response.body()!!
        } else {
            showErrorMessage()
            Log.e("ApiCallback", response.message())
        }
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        showErrorMessage()
        Log.e("ApiCallback", "Request Failure", t)
    }

    private fun showErrorMessage() {
        val context = AyudaPyApplication.getApplicationContext()
        Toast.makeText(context, R.string.api_error, Toast.LENGTH_LONG).show()
    }
}