package org.ayudapy.api

data class Feature(var type: String, var geometry: Geometry, var properties: Properties)