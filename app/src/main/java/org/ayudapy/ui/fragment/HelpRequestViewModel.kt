package org.ayudapy.ui.fragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import org.ayudapy.api.ApiBuilder
import org.ayudapy.api.ApiCallback
import org.ayudapy.api.HelpRequest
import org.ayudapy.db.AppDatabase
import org.ayudapy.db.CompletedItem
import org.ayudapy.db.SavedItem

class HelpRequestViewModel : ViewModel() {

    private val api = ApiBuilder.build()
    private val dao = AppDatabase.get().appDao()

    fun getHelpRequest(id: Int): LiveData<HelpRequest> {
        val liveData = MutableLiveData<HelpRequest>()
        api.getHelpRequest(id).enqueue(ApiCallback(liveData))
        return liveData
    }

    fun getSavedItems(): LiveData<List<SavedItem>> {
        return dao.getSavedItems()
    }

    fun getSavedItem(id: Int): LiveData<SavedItem?> {
        return dao.getSavedItem(id)
    }

    fun getCompletedItem(id: Int): LiveData<CompletedItem?> {
        return dao.getCompletedItem(id)
    }

    fun addFavorite(helpRequest: HelpRequest) {
        viewModelScope.launch {
            dao.insert(SavedItem(helpRequest))
        }
    }

    fun removeFavorite(item: SavedItem) {
        viewModelScope.launch {
            dao.delete(item)
        }
    }

    fun addCompleted(id: Int) {
        viewModelScope.launch {
            dao.insert(CompletedItem(id))
        }
    }

    fun removeCompleted(item: CompletedItem) {
        viewModelScope.launch {
            dao.delete(item)
        }
    }
}