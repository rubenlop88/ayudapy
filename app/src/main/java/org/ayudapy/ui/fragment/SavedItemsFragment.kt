package org.ayudapy.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chauthai.swipereveallayout.SwipeRevealLayout
import com.chauthai.swipereveallayout.ViewBinderHelper
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_saved_items.*
import org.ayudapy.R
import org.ayudapy.api.HelpRequest
import org.ayudapy.db.SavedItem
import java.text.SimpleDateFormat
import java.util.*


class SavedItemsFragment : BottomSheetDialogFragment() {

    companion object {
        fun show(manager: FragmentManager) {
            val fragment = SavedItemsFragment()
            fragment.show(manager, "")
        }
    }

    private lateinit var adapter: SavedItemsAdapter
    private val viewModel: HelpRequestViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        adapter = SavedItemsAdapter(onDeleteItemListener = {
            viewModel.removeFavorite(it)
        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_saved_items, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_view.adapter = adapter
        recycler_view.layoutManager = LinearLayoutManager(requireContext())
        viewModel.getSavedItems().observe(viewLifecycleOwner, Observer {
            adapter.setSavedItems(it)
            adapter.notifyDataSetChanged()
        })
    }

    private inner class SavedItemsAdapter(val onDeleteItemListener: (SavedItem) -> Unit) :
        RecyclerView.Adapter<SavedItemViewHolder>() {

        private val viewBinderHelper =
            ViewBinderHelper() // This helps to save/restore the open/close state of each view

        private var savedItems: MutableList<SavedItem>? = null

        init {
            viewBinderHelper.setOpenOnlyOne(true) // to open only one row at a time
        }

        fun setSavedItems(savedItems: List<SavedItem>) {
            this.savedItems = savedItems.toMutableList() // to delete item from list
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedItemViewHolder {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.saved_item, parent, false)
            return SavedItemViewHolder(view)
        }

        override fun getItemCount(): Int {
            return savedItems?.size ?: 0
        }

        override fun onBindViewHolder(holder: SavedItemViewHolder, position: Int) {
            val item = savedItems!![position]
            holder.savedItem = item
            holder.title.text = item.title
            holder.message.text = item.message
            holder.day.text = formatDateDay(item.added)
            holder.month.text = formatDateMonth(item.added)
            holder.deleteLayout.setOnClickListener {
                onDeleteItemListener(item)
                savedItems?.remove(item)
                notifyItemRemoved(position)
            }
            holder.cardView.setOnClickListener {
                HelpRequestFragment.show(childFragmentManager, item.id)
            }
            // Save/restore the open/close state.
            // You need to provide a String id which uniquely defines the data object.
            viewBinderHelper.bind(holder.swipeRevealLayout, item.id.toString());
        }

        private fun formatDateDay(date: String): String {
            val formatOutputDay = SimpleDateFormat("dd", Locale.getDefault())
            val timestamp = HelpRequest.dateFormat().parse(date)?.time
            return formatOutputDay.format(timestamp)
        }

        private fun formatDateMonth(date: String): String {
            val formatOutputMonth = SimpleDateFormat("MMM", Locale.getDefault())
            val timestamp = HelpRequest.dateFormat().parse(date)?.time
            val formatted = formatOutputMonth.format(timestamp)
            return formatted.toUpperCase(Locale.getDefault()).replace(".", "")
        }
    }

    private inner class SavedItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val swipeRevealLayout: SwipeRevealLayout = itemView.findViewById(R.id.swipe_layout)
        val deleteLayout: FrameLayout = itemView.findViewById(R.id.delete_layout)
        val cardView: FrameLayout = itemView.findViewById(R.id.card_view)
        val title: TextView = itemView.findViewById(R.id.title)
        val message: TextView = itemView.findViewById(R.id.message_view)
        val day: TextView = itemView.findViewById(R.id.date_day)
        val month: TextView = itemView.findViewById(R.id.date_month)
        var savedItem: SavedItem? = null
    }
}