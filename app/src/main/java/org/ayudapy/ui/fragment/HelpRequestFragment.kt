package org.ayudapy.ui.fragment

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.core.graphics.drawable.DrawableCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.ayudapy.R
import org.ayudapy.api.HelpRequest
import org.ayudapy.databinding.FragmentHelpRequestBinding
import org.ayudapy.db.CompletedItem
import org.ayudapy.db.SavedItem
import java.text.SimpleDateFormat
import java.util.*

class HelpRequestFragment : BottomSheetDialogFragment() {

    companion object {
        fun show(manager: FragmentManager, itemId: Int) {
            val fragment = HelpRequestFragment()
            val arguments = Bundle()
            arguments.putInt("id", itemId)
            fragment.arguments = arguments
            fragment.show(manager, "")
        }
    }

    private val permission = Manifest.permission.CALL_PHONE

    private lateinit var binding: FragmentHelpRequestBinding
    private val viewModel: HelpRequestViewModel by viewModels()

    private var itemId: Int = 0
    private var savedItem: SavedItem? = null
    private var completedItem: CompletedItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        itemId = arguments?.getInt("id")!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_help_request, container, false)
        binding.helpRequestGroup.visibility = View.INVISIBLE
        binding.progressBar.visibility = View.VISIBLE
        binding.phoneButton.setOnClickListener { checkCallPermission() }
        binding.addressButton.setOnClickListener { getDirections() }
        binding.shareButton.setOnClickListener { shareIntent() }
        binding.favoritesButton.setOnClickListener { addOrRemoveFavorite() }
        binding.completedButton.setOnClickListener { addOrRemoveCompleted() }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getHelpRequest(itemId).observe(viewLifecycleOwner, Observer { item ->
            updateUi(item)
        })
        viewModel.getSavedItem(itemId).observe(viewLifecycleOwner, Observer { item ->
            savedItem = item
            updateUi(item)
        })
        viewModel.getCompletedItem(itemId).observe(viewLifecycleOwner, Observer { item ->
            completedItem = item
            updateUi(item)
        })
    }

    private fun updateUi(request: HelpRequest) {
        formatDate(request)
        binding.helpRequest = request
        binding.progressBar.visibility = View.INVISIBLE
        binding.helpRequestGroup.visibility = View.VISIBLE
        binding.shareText.text = getString(R.string.share_text, request.name.split(" ")[0])
    }

    private fun updateUi(item: SavedItem?) {
        val id = if (item != null) R.drawable.ic_fav_remove else R.drawable.ic_fav_add
        val drawable = ContextCompat.getDrawable(requireContext(), id)
        binding.favoritesButton.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null)
        binding.favoritesButton.setText(if (item != null) R.string.fav_remove else R.string.fav_add)
    }

    private fun updateUi(item: CompletedItem?) {
        val id = if (item != null) R.color.green else R.color.light_gray
        val color = ContextCompat.getColor(requireContext(), id)
        val drawable = ContextCompat.getDrawable(requireContext(), R.drawable.ic_check)!!
        DrawableCompat.setTint(DrawableCompat.wrap(drawable), color)
        binding.completedButton.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null)
        binding.completedButton.setText(if (item != null) R.string.remove_completed else R.string.mark_completed)
    }

    private fun formatDate(helpRequest: HelpRequest) {
        val formatOutput = SimpleDateFormat(getString(R.string.added_date_format), Locale.getDefault())
        val timestamp = HelpRequest.dateFormat().parse(helpRequest.added)?.time
        helpRequest.addedFormatted = formatOutput.format(timestamp)
    }

    private fun getDirections() {
        val lat = binding.helpRequest!!.location.coordinates[1]
        val lon = binding.helpRequest!!.location.coordinates[0]
        val url = "https://www.google.com/maps/dir/?api=1&destination=$lat,$lon"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        startActivity(intent)
    }

    private fun checkCallPermission() {
        if (PackageManager.PERMISSION_GRANTED == checkSelfPermission(requireContext(), permission)) {
            callPhone()
        } else {
            requestPermissions(arrayOf(permission), 0)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        if (requestCode == 0) {
            if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                callPhone()
            }
            return
        }
    }

    private fun callPhone() {
        val number = binding.helpRequest!!.phone
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: $number"))
        startActivity(intent)
    }

    private fun shareIntent() {
        val parts = binding.helpRequest!!.name.split(" ")
        val extraText = getString(R.string.share_app, parts[0], "" + binding.helpRequest!!.id)
        val shareText = getString(R.string.share_via)
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = "text/plain"
        shareIntent.putExtra(Intent.EXTRA_TEXT, extraText)
        startActivity(Intent.createChooser(shareIntent, shareText))
    }

    private fun addOrRemoveFavorite() {
        if (savedItem == null) {
            viewModel.addFavorite(binding.helpRequest!!)
        } else {
            viewModel.removeFavorite(savedItem!!)
        }
    }

    private fun addOrRemoveCompleted() {
        if (completedItem == null) {
            showDialog(R.string.mark_completed_dialog) { viewModel.addCompleted(itemId) }
        } else {
            showDialog(R.string.remove_completed_dialog) { viewModel.removeCompleted(completedItem!!) }
        }
    }

    private fun showDialog(stringId: Int, callback: () -> Unit) {
        AlertDialog.Builder(requireContext())
            .setTitle(R.string.app_name)
            .setMessage(stringId)
            .setPositiveButton(android.R.string.yes) { _, _ -> callback() }
            .setNegativeButton(android.R.string.no) { _, _ -> dismiss() }
            .show()
    }
}