package org.ayudapy.ui.activity

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.os.Looper
import android.os.PersistableBundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.checkSelfPermission
import androidx.lifecycle.Observer
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnCameraMoveStartedListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.ClusterItem
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import kotlinx.android.synthetic.main.activity_map.*
import kotlinx.android.synthetic.main.menu_buttons.*
import org.ayudapy.R
import org.ayudapy.api.HelpRequests
import org.ayudapy.ui.fragment.HelpRequestFragment
import org.ayudapy.ui.fragment.SavedItemsFragment


class HelpRequestsActivity : AppCompatActivity(), OnMapReadyCallback, OnCameraMoveStartedListener {

    private val permission = Manifest.permission.ACCESS_FINE_LOCATION

    private lateinit var request: LocationRequest
    private lateinit var callback: LocationCallback
    private lateinit var client: FusedLocationProviderClient

    private lateinit var map: GoogleMap
    private lateinit var clusterManager: ClusterManager<Item>

    private val viewModel: HelpRequestsViewModel by viewModels()
    private var helpRequests: HelpRequests? = null
    private var savedItemIds: List<Int> = emptyList()
    private var completedItemIds: List<Int> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)
        setUpMenuButtons()
        setUpMapFragment()
        setUpObservers()
    }

    private fun setUpMenuButtons() {
        menu_fab.setOnClickListener { showMenuButtons() }
        info_button.setOnClickListener { InfoActivity.start(this) }
        help_text.setOnClickListener { WebActivity.start(this, "https://ayudapy.org/recibir") }
        favs_button.setOnClickListener { SavedItemsFragment.show(supportFragmentManager) }
    }

    private fun setUpMapFragment() {
        val fragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        fragment.getMapAsync(this)
    }

    private fun setUpObservers() {
        viewModel.getSavedItems().observe(this, Observer { items ->
            savedItemIds = items.map { it.id }
            showMarkers()
        })
        viewModel.getCompletedItems().observe(this, Observer { items ->
            completedItemIds = items.map { it.id }
            showMarkers()
        })
    }

    private fun showMenuButtons() {
        menu_layout.visibility = View.VISIBLE
        menu_fab.animate().scaleX(0f).scaleY(0f)
        menu_layout.animate().scaleX(1f).scaleY(1f)
    }

    override fun onCameraMoveStarted(reason: Int) { // hide menu buttons
        menu_layout.animate().scaleX(0f).scaleY(0f)
        menu_fab.animate().scaleX(1f).scaleY(1f)
        menu_layout.visibility = View.GONE
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.setPadding(0, 120, 0, 0)
        setUpClusterManager()
        setupLocationRequest()
        setUpLocationButton()
    }

    private fun setUpClusterManager() {
        clusterManager = ClusterManager<Item>(this, map)
        clusterManager.renderer = ItemRenderer(this, map, clusterManager)
        clusterManager.setOnClusterItemClickListener(ItemClickListener())
        map.setOnCameraIdleListener(clusterManager)
        map.setOnMarkerClickListener(clusterManager)
    }

    private fun setupLocationRequest() {
        request = LocationRequest()
        client = LocationServices.getFusedLocationProviderClient(this)
        callback = object : LocationCallback() {
            override fun onLocationResult(result: LocationResult) {
                super.onLocationResult(result)
                client.removeLocationUpdates(this)
                onLocationResult(result.lastLocation)
            }
        }
    }

    private fun onLocationResult(location: Location) {
        val latLng = LatLng(location.latitude, location.longitude)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
        map.setOnCameraMoveStartedListener(this)
    }

    private fun setUpLocationButton() {
        if (PackageManager.PERMISSION_GRANTED == checkSelfPermission(this, permission)) {
            map.isMyLocationEnabled = true
            client.requestLocationUpdates(request, callback, Looper.getMainLooper())
        } else {
            map.isMyLocationEnabled = false
            ActivityCompat.requestPermissions(this, arrayOf(permission), 0)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, results: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, results)
        if (requestCode == 0) {
            if (results.isNotEmpty() && results[0] == PackageManager.PERMISSION_GRANTED) {
                setUpLocationButton()
            }
        }
    }

    private fun getHelpRequests() {
        val ne = map.projection.visibleRegion.latLngBounds.northeast
        val sw = map.projection.visibleRegion.latLngBounds.southwest
        val box = "${ne.longitude},${ne.latitude},${sw.longitude},${sw.latitude}"
        val liveData = viewModel.getHelpRequests(box)
        liveData.observe(this, Observer {
            liveData.removeObservers(this)
            helpRequests = it
            showMarkers()
        })
    }

    private fun showMarkers() {
        if (helpRequests == null) {
            return
        }
        val items = mutableListOf<Item>()
        for (feature in helpRequests?.features!!) {
            val id = feature.properties.pk
            val name = feature.properties.name.split(" ")[0]
            val title = getString(R.string.marder_title, name)
            val coordinates = feature.geometry.coordinates
            val position = LatLng(coordinates[1], coordinates[0])
            val item = Item(id, position, title)
            items.add(item)
        }
        clusterManager.clearItems()
        clusterManager.addItems(items)
        clusterManager.cluster()
    }

    private fun getDrawableId(id: Int): Int {
        return when {
            completedItemIds.contains(id) -> R.drawable.pin_checked
            savedItemIds.contains(id) -> R.drawable.pin_fav
            else -> R.drawable.pin
        }
    }

    private inner class ItemClickListener : ClusterManager.OnClusterItemClickListener<Item> {

        override fun onClusterItemClick(item: Item): Boolean {
            HelpRequestFragment.show(supportFragmentManager, item.id)
            return true
        }
    }

    private inner class ItemRenderer(context: Context, map: GoogleMap, manager: ClusterManager<Item>) :
        DefaultClusterRenderer<Item>(context, map, manager), GoogleMap.OnCameraIdleListener {

        override fun onBeforeClusterItemRendered(item: Item, options: MarkerOptions) {
            val resId = getDrawableId(item.id)
            val bitmap = BitmapDescriptorFactory.fromResource(resId)
            options.icon(bitmap)
        }

        override fun onCameraIdle() {
            getHelpRequests()
        }
    }

    private inner class Item(val id: Int, private val position: LatLng, private val title: String) : ClusterItem {

        override fun getPosition(): LatLng {
            return position
        }

        override fun getTitle(): String? {
            return title
        }

        override fun getSnippet(): String? {
            return null
        }
    }
}
