package org.ayudapy.ui.activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import org.ayudapy.api.ApiBuilder
import org.ayudapy.api.ApiCallback
import org.ayudapy.api.HelpRequests
import org.ayudapy.db.AppDatabase
import org.ayudapy.db.CompletedItem
import org.ayudapy.db.SavedItem

class HelpRequestsViewModel : ViewModel() {

    private val api = ApiBuilder.build()
    private val dao = AppDatabase.get().appDao()

    fun getHelpRequests(boundary: String): LiveData<HelpRequests> {
        val liveData = MutableLiveData<HelpRequests>()
        api.getHelpRequests(boundary).enqueue(ApiCallback(liveData))
        return liveData
    }

    fun getSavedItems(): LiveData<List<SavedItem>> {
        return dao.getSavedItems()
    }

    fun getCompletedItems(): LiveData<List<CompletedItem>> {
        return dao.getCompletedItems()
    }
}