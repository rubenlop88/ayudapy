package org.ayudapy.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_info.*
import org.ayudapy.R

class InfoActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context) {
            val intent = Intent(context, InfoActivity::class.java)
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info)
        terms_label.setOnClickListener {
            WebActivity.start(this, "https://ayudapy.org/legal")
        }
        faq_label.setOnClickListener {
            WebActivity.start(this, "https://ayudapy.org/preguntas_frecuentes")
        }
        ayudapy_label.setOnClickListener {
            WebActivity.start(this, "https://ayudapy.org")
        }
        gitlab_label.setOnClickListener {
            WebActivity.start(this, "https://gitlab.com/rubenlop88/ayudapy")
        }
    }
}
