package org.ayudapy

import android.content.Context
import org.ayudapy.db.AppDatabase

class AyudaPyApplication : android.app.Application() {

    companion object {
        private lateinit var context: Context

        fun getApplicationContext() : Context {
            return context
        }
    }

    override fun onCreate() {
        super.onCreate()
        AppDatabase.init(this)
        context = this
    }
}